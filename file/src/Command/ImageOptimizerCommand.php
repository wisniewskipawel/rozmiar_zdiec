<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Repository\FilesRepository;
use App\Service\FilesService;


class ImageOptimizerCommand extends Command
{
    protected static $defaultName = 'app:image-otimizer';
    protected static $defaultDescription = 'Add a short description for your command';

    private $filesRepository;    
    private $filesService;
    
    public function __construct(FilesRepository $filesRepository, FilesService $filesService)
    {
        $this->filesRepository = $filesRepository;
        $this->filesService = $filesService;
        parent::__construct();
    }
    
    protected function configure(): void
    {
        $this
        ->addArgument('catalog', InputArgument::OPTIONAL, 'catalog');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        
        $catalog = $input->getArgument('catalog');
        $this->filesService->createMiniature($catalog);        
        $io->success('I created thumbnails.');

        return Command::SUCCESS;
    }
}
