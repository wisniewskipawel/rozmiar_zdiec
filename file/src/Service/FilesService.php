<?php
namespace App\Service;

use Symfony\Component\HttpFoundation\File\Exception\FileException;
use App\Repository\FilesRepository;
use Imagine\Gd\Imagine;
use Imagine\Image\Box;

class FilesService {
    
    private $filesRepository;
    
    private $width = 150;
    private $height = 150;
   
    private $catalog = 'wminiatury';
    
    private $imagine;
    
    public function __construct(FilesRepository $filesRepository)
    {
        $this->filesRepository = $filesRepository;
        $this->imagine = new Imagine();
    }

    public function uploadFile($file,$path,$newName)
    {
        try{

            $original = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $new = $newName.'-'.uniqid().'.'.$file->guessExtension();
            $file->move($path,$new);
            $this->filesRepository->add($path,$new);

        }catch(FileException $e)
        {
            throw new \Exception('Unable to remove file from ' .$path.$new);
            return $e->getMessage();
        }
    }
    
    public function createCatalog($path)
    {
        if(!is_dir($path.'/'.$this->catalog))
        {
            $newPath = mkdir($path.'/'.$this->catalog);
            
            return $newPath;
        }
        return $path.'/'.$this->catalog;
                  
    }
    
    public function createMiniature($catalog)
    {
        $files = $this->filesRepository->getNameFile();

        if($catalog)
         {
            $this->catalog = $catalog;
         }

        $imagine = new Imagine();
          
        foreach($files as $value)
        {
            $newPath = $this->createCatalog($value['photo']) ? $this->createCatalog($value['photo']) : $value['photo'].'/'.$this->catalog;
            $realImage = $imagine->open($value['photo'].'/'.$value['name']);
            $realImage->resize(new Box($this->width, $this->height))
                       ->save($newPath.'/'.$value['name']);
         }                    
    }
}
?>