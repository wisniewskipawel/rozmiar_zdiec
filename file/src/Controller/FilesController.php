<?php

namespace App\Controller;

use App\Entity\Files;
use App\Form\FilesType;
use App\Repository\FilesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\FilesService;

use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
/**
 * @Route("/files")
 */
class FilesController extends AbstractController
{
    
    private $filesRepository;
    
    public function __construct(FilesRepository $filesRepository)
    {
        $this->filesRepository = $filesRepository;
    }
    
    /**
     * @Route("/", name="files_index", methods={"GET"})
     */
    public function index(): Response
    {
        return $this->render('files/index.html.twig', [
            'files' => $this->filesRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="files_new", methods={"GET", "POST"})
     */
    public function new(Request $request, FilesService $filesService): Response
    {
        $file = new Files();
        $form = $this->createForm(FilesType::class, $file);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            if($file = $form->get('photo')->getData())
            {
                $newName = $form['name']->getData();            
                $path = $this->getParameter('photoDir');
                $filesService->uploadFile($file, $path,$newName);
            }
            
            $this->addFlash('success', 'The file has been added');
            return $this->redirectToRoute('files_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('files/new.html.twig', [
            'file' => $file,
            'form' => $form,
        ]);
    }

}